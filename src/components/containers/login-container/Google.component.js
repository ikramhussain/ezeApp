import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { googleUser } from './Login.actionCreators';

import ImgGoogle from'../../presentationals/Login-presentational/images/GoogleIcon.png';
import'../../presentationals/Login-presentational/css/Login.css';

class GoogleLogin extends Component {


    GOOGLE_API_KEYS = {
		CLIENT_ID: '284197977688-3unitkrn2vgmnc0a2o9fvm07tcfd5itc.apps.googleusercontent.com', // Client ID and API key from the Developer Console
		DISCOVERY_DOCS: [ 'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'], 	// Array of API discovery doc URLs for APIs used by the quickstart
		SCOPES: 'https://www.googleapis.com/auth/calendar  https://www.google.com/m8/feeds'	// Authorization scopes required by the API; multiple scopes can be included, separated by spaces.
	};

    render() {
        return (
             <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                 <a >
                     <img src={ImgGoogle} className="center-block" onClick={() => {
                                        gapi.auth2.getAuthInstance().signIn();
            }} /></a></div>           
        );
    }


    componentDidMount() {
        // loading the script at run time (google calendar api).		
        const script = document.createElement("script");
        script.src = "https://apis.google.com/js/api.js";
        script.onload = this.handleClientLoad.bind(this);
        // script.onreadystatechange = "if (this.readyState === 'complete') this.onload()";
        script.async = true;
        document.body.appendChild(script);
    }
    

    handleClientLoad() {
        gapi.load('client:auth2', initClient);
        const componentRef = this;

        function initClient() {
            gapi.client.init({
                discoveryDocs: componentRef.GOOGLE_API_KEYS.DISCOVERY_DOCS,
                clientId: componentRef.GOOGLE_API_KEYS.CLIENT_ID,
                scope: componentRef.GOOGLE_API_KEYS.SCOPES,
                //	 scope: "profile email"
            }).then(function () {
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            });


            function updateSigninStatus(isSignedIn) {
                if (isSignedIn) {
                    console.log('Auth Response', gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse());
                    const ACCESS_TOKEN = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
                    const ID_TOKEN = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token;
                    const id = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getId();
                    const Name = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getName();
                    const email = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail();
                    const image = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getImageUrl();
                     sessionStorage.setItem('acunttype',"Google");
                     sessionStorage.setItem('useremail',email);
                     sessionStorage.setItem('userid',id);
                      sessionStorage.setItem('username',Name);
                    componentRef.props.handleClick(Name, email, ACCESS_TOKEN,() => {
                      alert("jhjshh")
                        browserHistory.push('Home');
                        
                    });
                    //componentRef.google_data();

                } else {
                    console.log('please sign in....')
                    /*authorizeButton.style.display = 'block';
                    signoutButton.style.display = 'none';*/
                }
            }
        }
    }

}

const mapDispatchToProps = dispatch => {
    return {
        handleClick: (name, email, access_token) => dispatch(googleUser(name, email, access_token))
    };
};

export default connect(null, mapDispatchToProps)(GoogleLogin);