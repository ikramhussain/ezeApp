import { browserHistory } from 'react-router';

import LocalStorage from '../../../utils/localStorageUtil';

const ACCESS_TOKEN = 'ACCESS_TOKEN';

const gettingEvents = () => {
    return {
        type: 'FETCHING_EVENTS'
    }
};

export const getEvents = (userId) => {
    console.log("userid" + userId)
    gettingEvents();

    return dispatch => {

        fetch('http://192.168.0.101:8070/events/calendar/' + userId)
            .then(res => res.json())
            .then(json => {
                const userEvents = []
                json.map((event) => {
                    userEvents.push({
                        start: new Date(event.startTime),
                        end: new Date(event.endTime),
                        title: event.eventName,
                        desc: event.eventDescription,
                        eventLocation:event.eventLocation,
                        eventAttendee:event.eventAttendee,
                        accountType:event.accountType,
                        emailId:event.emailId,
                        eventId:event.eventId
                    })
                })
                dispatch({
                    type: 'ADD_EVENTS',
                    events: userEvents

                })
            })
            .catch(err => console.log(err));
    };
};

export const loginAction = (username, paasword) => {
    return dispatch => {
        var data = {

            "emailId": username,
            "password": paasword,

        }


        fetch("http://192.168.0.101:8070/user/signin", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.userId != null) {

                    const { userId, name, emailId: email } = responseJson;
                    sessionStorage.setItem('userId', responseJson.userId);
                    LocalStorage.saveInLocalStorage(ACCESS_TOKEN, userId);

                    browserHistory.push('/Home');
                    dispatch({
                        type: 'ResponceUserData',
                        userDetails: {
                            userId,
                            name,
                            email
                        }
                    })

                }
                else {
                    alert("Invalid User Name And Password");
                }
            }).catch(function (err) {
                console.log(err);
            })
    };

}
export const RegisterUser = (username, email, paasword, callback) => {
   // console.log(username, email, paasword)

    console.log('Registring in action cretror', callback);
    return dispatch => {
        var data = {
            "name": username,
            "emailId": email,
            "password": paasword,

        }

        fetch("http://192.168.0.101:8070/user/signup", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
               
                let userid=responseJson.userId;
                if (userid != null) {
                    console.log("hsajdfh"+responseJson.userId);
                    const { userId, name, emailId: email } = responseJson;
                    sessionStorage.setItem('userId', responseJson.userId);

                    callback('Registration Successfull!!');
                     alert("You Have successfully Register")
                    dispatch({
                        type: 'ResponceUserData',
                        userDetails: {
                            userId,
                            name,
                            email
                        }
                    })
                }
            }).catch(function (err) {
                callback('Registration Failed!!');
                console.log(err);
            })
    };


}
export const googleUser = (name, email, access_token, loginCallback) => {
    console.log("google" + name, email, access_token, loginCallback);
    return dispatch => {

        var data = {
            "accessToken": access_token,
            "emailId": email,
            "accountType": "Google",
            "name": name,
            "userId": ''
        }

        console.log('data', data);

        fetch("http://192.168.0.101:8070/user/access", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                //console.log(responseJson[0].result);
                console.log(responseJson);
                if (responseJson.userId != null) {
                    console.log("hjbdzfhgdshsv"+responseJson.userId);
                     const { userId, name, emailId: email } = responseJson;
                     
                    sessionStorage.setItem('userId', responseJson.userId);
                  
                    LocalStorage.saveInLocalStorage(ACCESS_TOKEN, access_token);
                    browserHistory.push('/Home');
                    //loginCallback();
                          dispatch({
                        type: 'ResponceUserData',
                        userDetails: {
                            userId,
                            name,
                            email
                        }
                    })

                }
            }).catch(function (err) {
                console.log(err);
            })
    };

}
export const outlookUser = (access_token) => {
    console.log("outlook")
    return dispatch => {

        var data = {
            "accessToken": access_token,
            "accountType": "Outlook",
            "userId": ""
        }

        console.log('data', data);

        fetch("http://192.168.0.101:8070/user/access", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("hellofijdsfijsd" + responseJson)
                if (responseJson.userId != null) {
 const { userId, name, emailId: email } = responseJson;
                    sessionStorage.setItem('userId', responseJson.userId);
                   
                    LocalStorage.saveInLocalStorage(ACCESS_TOKEN, access_token);
                    browserHistory.push('/Home');
                    //loginCallback();
                        dispatch({
                        type: 'ResponceUserData',
                        userDetails: {
                            userId,
                            name,
                            email
                        }
                    })
                }
            }).catch(function (err) {
                console.log(err);
            })
    };

}
export const forgotPasswordAction = (email) => {
    console.log("userid" + email)
    
    return dispatch => {
        var data = {
             "emailId": email,
             "password":' '
             }

        fetch("http://192.168.0.101:8070/user/password", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson[0].result =='Success') {
                    browserHistory.push('/');
                   alert("Check Your Email Id");
                   }
            }).catch(function (err) {
                console.log(err);
            })
    };
};
