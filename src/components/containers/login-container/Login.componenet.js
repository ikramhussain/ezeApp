import React, { Component } from 'react';
import Log from '../../presentationals/Login-presentational/LoginPanel.component'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Grid, Navbar, Jumbotron, Button } from 'react-bootstrap';
import '../../presentationals/Login-presentational/css/Login.css';

import Calendar from '../../presentationals/Login-presentational/images/Calendar.png';
import WhiteLogo from '../../presentationals/Login-presentational/images/logowithtext.png';

import RegisterPanel from '../../presentationals/Login-presentational/RegisterPanel.component';
//import '../../presentational/login-presentational/style.scss';

import { loginAction, RegisterUser,forgotPasswordAction} from './Login.actionCreators';
import { isAuthenticated } from '../../../utils/AuthUtils';
import {validateTextField,validateRegistrationTextField,validateEmailVarify} from '../../../utils/validation';
class Login extends Component {
    state = {
        userEmail: '',
        paasword: '',
        repassword: '',
        email: '',
        registrationStatus: ''
      
    };
    
   
    Registration = (event,usernameInput, userEmailInput, userPasswordInput,userRepasswordInput) => {
         
      
 validateRegistrationTextField(usernameInput,userEmailInput,userPasswordInput,userRepasswordInput,(callback) => {
      if(callback==true)
      {
          this.props.userRegistration(usernameInput, userEmailInput, 
          userPasswordInput,(status) => {
            console.log('registering, ', status);
    
           this.setState({ registrationStatus: status });
                       });
      }
      else{

      }
    });
    }
    login = (event,  userEmailInput, password) => {
        console.log( userEmailInput, password);
       
     validateTextField(userEmailInput,password, (callback) => {
      if(callback==true)
      {
          this.props.userLogin(userEmailInput,password);
      }
      else{

      }
    });
       
    }
   forgotUserPassword=(event,emailVarification)=>{
       console.log(emailVarification);
       validateEmailVarify(emailVarification,(callback)=>{
    if(callback==true){
     this.props.passwordSend(emailVarification);
    }})

   }
    render() {
        return (
            <div className="eze-login-container">
                <div className="col-lg-8 col-md-8 col-sm-6 hidden-xs eze-login-banner no-padding">
                    <div className="eze-logo">
                        <img src={WhiteLogo} />
                    </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding eze-signin-panel">
                    <ul className="nav nav-tabs">
                        <li className="eze-login-li active">
                            <a data-toggle="tab" href="#login" ><b>Login</b></a></li>
                        <li className="eze-login-li"><a data-toggle="tab"
                         href="#register"><b>Register</b></a></li>
                    </ul>
                    <div className="tab-content">
                        <div id="login" className="tab-pane fade in active">
                            <Log  
                                 login={this.login} 
                               
                                 forgotUserPassword={this.forgotUserPassword}
                                 />
                        </div>
                        <div id="register" className="tab-pane fade">
                            <RegisterPanel 
                                registerationStatus={this.state.registrationStatus}
                                 Registration={this.Registration} 
                                 />
                        </div>
                    </div>
                </div>
            </div>

        );
    }

    componentDidMount() {
        if(isAuthenticated())
            browserHistory.push('/Home');
    }

}

 

const mapDispatchToProps = dispatch => {
    return {
       userRegistration: (userEmail, email,paasword, callback) => dispatch(RegisterUser(userEmail, email,paasword, callback)),
          userLogin: (userEmail,paasword) => dispatch(loginAction(userEmail,paasword)),
          passwordSend:(forgotPassword)=> dispatch(forgotPasswordAction(forgotPassword))
    };
};




export default connect(null, mapDispatchToProps)(Login)




