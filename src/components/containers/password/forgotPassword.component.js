import React, { Component } from 'react';
import Forgotpassword from '../../presentationals/password/ForgotPassword.component';
 import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {forgotPasswordAction} from './Password.actionCreater';

let userId;
class password extends Component {
    componentDidMount(){
userId = getParameterByName('id');
console.log("serverget"+userId)
      function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
    }
    render() {

        return (
           	<div>
				<Forgotpassword passwordSubmit={this.passwordSubmit}  />
                
			</div>
        )
    };
 passwordSubmit=(event,password,confirm_password)=>{
    
  this.validatePassword(password,confirm_password);
}
validatePassword(password,confirm_password){
  if(password != confirm_password) {
  
   alert("Password Not Match");
    window.location.reload();
  } else {
    
    this.props.passwordSend(userId,password);
  }
}
}
const mapDispatchToProps = dispatch => {
    return {
       passwordSend:(userId,password)=> dispatch(forgotPasswordAction(userId,password))
    };
};
export default connect(null, mapDispatchToProps)(password)
