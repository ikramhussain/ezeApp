import { browserHistory } from 'react-router';

import LocalStorage from '../../../utils/localStorageUtil';

export const MeetingCreate = (MettingTitel, Sdate, Stime, Etime, Description, Slocation, participantdata, AccontType) => {

    return dispatch => {
        var data = {
            "eventName": MettingTitel,
            "startDate": Sdate + "T" + Stime,
            "endDate": Sdate + "T" + Etime,
            "emailId": sessionStorage.getItem('useremail'),
            "eventDescription": Description,
            "eventLocation": Slocation,
            "eventAttendee": participantdata,
            "accountType": AccontType
        }
        console.log('dataaaaa', data);
        fetch("http://192.168.0.101:8070/event/createEvent", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })

            .then((response) => response.json())
            .then((responseJson) => {
                console.log("respo" + responseJson[0].result);
                if (responseJson[0].result == 'Success') {
                    alert("Your Event Successfully Saved");
                    window.location.reload();
                }
            }).catch(function (err) {
                return err;
            })
    };

}
export const DeleteEvent = (eventId) => {

    return dispatch => {
       
        console.log('dataaaaa', eventId);
        fetch("http://192.168.0.101:8070/events/calenderEvent/"+eventId, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })

            .then((response) => response.json())
            .then((responseJson) => {
                console.log("respo" + responseJson[0].result);
                if (responseJson[0].result == 'Success') {
                   
                    browserHistory.push('/')
                }
            }).catch(function (err) {
                return err;
            })
    };

}
export const logout = () => {
    console.log('logging out');

    LocalStorage.deleteFromLocalStorage('ACCESS_TOKEN');

    return dispatch => {

        // if successfull
          
        browserHistory.push('/');
        sessionStorage.removeItem('idToken');
           sessionStorage.removeItem('accessToken');
        gapi.auth2.getAuthInstance().signOut();

        dispatch({
            type: 'LOGOUT'
        })
    };
};

