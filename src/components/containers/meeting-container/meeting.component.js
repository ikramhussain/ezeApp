import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import MeetingHeader from '../../presentationals/createMeeting-presentational/SideNavigation.component';

import { logout } from './Meeting.actionCreators';

class MeetingContainer extends Component {
    render() {
        return(
            <div className="eze-meeting-container" style={{height: '100%'}}>
               <MeetingHeader logoutHandler={this.props.logoutHandler} />
            </div>
        )
    }
}

const mapStateToProps = ({}) => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        logoutHandler: logout
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(MeetingContainer);