
import React, { Component } from 'react';
import Connect from 'react-redux';
import EditEvent from '../../presentationals/createMeeting-presentational/EditEvent.component';
import { connect } from 'react-redux';
import { DeleteEvent } from './Meeting.actionCreators';
import { browserHistory } from 'react-router';

class Cmeeting extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            pardata: [],
             participantdata: [],
            MettingTitel:'',
            Sdate:'',
            Stime:'',
            Etime:'',
            Description:'',
            Slocation:'',
            AccontType:'',
            meetings:[],
            eventID:''
       }
      }
      componentDidMount(){
    var eventDetail = this.props.location.state.event
    console.log('view detail', eventDetail)
      this.setState({ meetings: eventDetail });
     
      this.setState({eventID:this.state.meetings.eventID})
  }
    render() {
       return (
            <div className="eze-meeting-container" style={{ height: '100%' }}>
              < EditEvent 
              addTodo={this.addTodo}
              removeuser={this.removeuser} 
              mettingTitel={this.mettingTitel} 
              sdate={this.sdate} stime={this.stime}
              etime={this.etime} 
              description={this.description}
              slocation={this.slocation}
              acntType={this.acntType} 
              create={this.create}  
              datee={this.state.Sdate}
              pardata={this.state.pardata}
              meetings={this.state.meetings}
              deleteUserEvent={this.deleteUserEvent}/>
                 </div>
          );
    }
    deleteUserEvent=()=>{
this.props.eventDelete(this.state.eventID);
    }
mettingTitel= (event) => {
        this.setState({ MettingTitel: event.target.value })
        console.log(this.state.MettingTitel)
    };
sdate= (event) => {
    console.log(event)
    var date = new Date(event._d);  //or your date here
    let snewdate =((date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear());
    date.setDate(date.getDate());
        var startDate = date.toISOString().slice(0, 10);
        this.setState({Sdate:startDate})
      
    };
   
stime= (event) => {
     console.log(event._d)
      var date = new Date(event._d)
       let snewtime=(date.getHours()+':'+(`0${date.getMinutes()}`).slice(-2)+':' +(`0${date.getMinutes()}`).slice(-2));
       this.setState({Stime:snewtime})
       
    };
etime= (event) => {
       
     var date = new Date(event._d)
       let snewEndtime=(date.getHours()+':'+(`0${date.getMinutes()}`).slice(-2)+':' +(`0${date.getSeconds()}`).slice(-2));
       this.setState({Etime:snewEndtime})
    };
description= (event) => {
        this.setState({ Description: event.target.value })
        console.log(this.state.Description)
    };
slocation= (event) => {
        this.setState({Slocation: event.target.value })
        console.log(this.state.Slocation)
    };
    acntType= (event) => {
        
        this.setState({AccontType: event.target.value })
       };
       
      
create= (event) => {
      const componentRef = this;
      componentRef.props.createMeetingClick(this.state.MettingTitel, this.state.Sdate, this.state.Stime, this.state.Etime,this.state.Description,this.state.Slocation,this.state.participantdata,this.state.AccontType);  
   event.preventDefault();
 };
    addTodo = (event, val, input) => {

        const componentRef = this;
        // Assemble data
          console.log("event"+event.target.value)
        if (event.key == 'Enter') {
            console.log(val);
            const todo = { text: val, id: val }
          
            this.state.participantdata.push(val);
            this.state.pardata.push(todo);
            // Update state
            this.setState({ pardata: this.state.pardata });
             this.setState({ participantdata: this.state.participantdata});
              console.log(this.state.participantdata);
            console.log(this.state.pardata[0].text);
            input.value = '';
            event.preventDefault();


        }
    }
 removeuser=(event,userlist,id)=>{
    const remainder = this.state.pardata.filter((userlist) => {
      if(userlist.id !== id) return userlist;
    });
   
    this.setState({pardata: remainder});
  }

}
const mapDispatchToProps = dispatch => {
    return {

        //createMeetingClick: (MettingTitel,Sdate,Stime,Etime,Description,Slocation,participantdata,AccontType) => dispatch(MeetingCreate(MettingTitel,Sdate,Stime,Etime,Description,Slocation,participantdata,AccontType)),
        eventDelete:(eventid)=> dispatch(DeleteEvent(eventId))
};
};

export default connect(null, mapDispatchToProps)(Cmeeting)
