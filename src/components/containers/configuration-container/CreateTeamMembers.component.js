import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import moment from 'moment';
import CreateTeam from '../../presentationals/Configuration-presentational/CreateTeamMember.component';
import ProductLine from '../../presentationals/Configuration-presentational/ProductLine.component';
// import Createmeeting from
// '../../presentationals/createMeeting-presentational/CreateMeeting.component';
import List from '../../presentationals/Configuration-presentational/CreateTeamMember.component'
import {EmployeeDetail,ProductDetail} from './Configuration.actionCreators';
class CreateMeetings extends Component {
   
     

   
    render() {
        return (
            <div className="container-fluid">
                <div className="container col-sm-3">
                    <h3>Add Configuration</h3>
                    <ul className="nav nav-pills nav-stacked">
                        <li className="active">
                            <a data-toggle="pill" href="#createTeamMember">Create Team Member</a>
                        </li>
                        <li>
                            <a data-toggle="pill" href="#productLine">Create Product</a>
                        </li>
                    </ul>
                </div>
                <div className="tab-content col-sm-9">
                    <div id="createTeamMember" className="tab-pane fade in active">
                        <CreateTeam
                         submit={this.submit}/>
                    </div>
                    <div id="productLine" className="tab-pane fade">
                        <ProductLine
                        addProductLine={this.addProductLine}/>
                    </div>
                </div>
               
            </div>
        )
    }
     addProductLine = (event,productName, productDescription, productFile,productPeriod) => {
 this.props .handleClickProduct(productName, productDescription, productFile,productPeriod);
}
 submit = (event,companyName, employeeName, employeeId,emailId,startTime,endTime,mobilenumber,workingDays) => {
        console.log("hiii"+workingDays);
        event.preventDefault();
        this.props .handleClick(companyName, employeeName,
         employeeId,emailId,startTime,endTime,mobilenumber,workingDays);
         }

}
const mapDispatchToProps = dispatch => {
    return {
        handleClick: (companyName, employeeName, employeeId, emailId, startTime, endTime, mobilenumber, workingDays) => dispatch(EmployeeDetail(companyName, employeeName, employeeId, emailId, startTime, endTime, mobilenumber, workingDays)),
        handleClickProduct: (productName, productDescription, productFile, productPeriod) => dispatch(ProductDetail(productName, productDescription, productFile, productPeriod))
 };
};
export default connect(null, mapDispatchToProps)(CreateMeetings)