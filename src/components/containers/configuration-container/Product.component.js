import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import moment from 'moment';
import ProductLine from '../../presentationals/Configuration-presentational/ProductLine.component';
import {ProductDetail} from './Configuration.actionCreators';
class Product extends Component {
    state = {
        productName: '',
        productDescription: '',
        productFile: [],
        productPeriod: '',
      
    };
    productName = (event) => {
        this.setState({productName: event.target.value})
        console.log(this.state.productName)
    };
    productDescription = (event) => {
        this.setState({productDescription: event.target.value})
        console.log(this.state.productDescription)
    }
    productFile = (event) => {
       this
            .state
            .productFile
            .push(event.target.value)
        this.setState({productFile: this.state.productFile})
        console.log(this.state.productFile)
    }
    productPeriod = (event) => {
        this.setState({productPeriod: event.target.value})
        console.log(this.state.productPeriod)
    }

    submit = (event) => {
        event.preventDefault();
        const componentRef = this;
        componentRef
            .props
            .handleClick(this.state.productName, this.state.productDescription, this.state.productFile, this.state.productPeriod,  () => {});
    }

    render() {
        return (
            <div className="container-fluid">
                    <div id="productLine" className="tab-pane fade">
                        <ProductLine
                            productName={this.productName}
                            productDescription={this.productDescription}
                            productFile={this.productFile}
                            productPeriod={this.productPeriod}
                            submit={this.submit}
                        />
                    </div>
               </div>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        handleClick: (productName, productDescription, productFile, productPeriod) => dispatch(ProductDetail(productName, productDescription, productFile, productPeriod))
    };
};
export default connect(null, mapDispatchToProps)(Product)