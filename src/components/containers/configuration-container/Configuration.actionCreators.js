export const EmployeeDetail = (companyName, employeeName, employeeId, emailId, startTime, endTime, mobilenumber, workingDays) => {
    console.log(companyName)
    return dispatch => {
        var data = {
            "employeeCompanyName": companyName,
            "employeeId": employeeId,
            "employeeName": employeeName,
            "employeeEmailId": emailId,
            "employeeWorkingStartTime": startTime,
            "employeeWorkingEndTime": endTime,
            "employeeContactNumber": mobilenumber,
            "employeeWorkingDays": workingDays
        }

        console.log('dataaaaa', data.employeeWorkingDays);
        fetch("http://192.168.0.101:8070/eze/employee", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json()).then((responseJson) => {
            console.log(responseJson);
            if (responseJson[0].result = 'Success') {
                window
                    .location
                    .reload();

                }
            })
            .catch(function (err) {
                console.log(err);
            })
    };
}

export const ProductDetail = (productName, productDescription, productFile, productPeriod) => {
    console.log(productName)
    return dispatch => {
        var data = {
            "productName": productName,
            "productDescription": productDescription,
            "productFile": productFile,
            "productPeriod": productPeriod
        }

        console.log('dataaaaa', data.productFile);
        fetch("http://192.168.0.101:8070/product/product_dtail", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.json()).then((responseJson) => {
            console.log(responseJson);
            if (responseJson[0].result = 'Success') {
                window
                    .location
                    .reload();

                }
            })
            .catch(function (err) {
                console.log(err);
            })
    };
}