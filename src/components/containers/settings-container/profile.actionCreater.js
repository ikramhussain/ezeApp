import { browserHistory } from 'react-router';
export const saveUser_Profile = (useremail,timezone) => {
    console.log("userid" + useremail, timezone)
   

    return dispatch => {
        var data = {
             "emaiId": useremail,
             "timeZone":timezone
             }

        fetch("http://192.168.0.101:8070/user/timezone", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson[0].result =='Success') {
                window.location.reload();
                  
                   }
            }).catch(function (err) {
                console.log(err);
            })
    };
};
export const saveUserNotification = (useremail,checkbox1,checkbox2,checkbox3,checkbox4) => {
    console.log("userid" + useremail,checkbox1,checkbox2,checkbox3,checkbox4)
   

    return dispatch => {
        var data = {
             "userId": useremail,
               "dailyMeetings": checkbox1,
                 "meetingStartEmail": checkbox2,
                  "meetingTips": checkbox3,
                   "meetingFeedback":checkbox4,
                   
             }

        fetch("http://192.168.0.101:8070/user/addNotification", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson[0].result =='Success') {
                window.location.reload();
                  
                   }
            }).catch(function (err) {
                console.log(err);
            })
    };
};