import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Notification from'../../presentationals/Settings-presentational/notifications.component';
 import {saveUserNotification} from './profile.actionCreater'
let useremailId;
 class notification extends Component {
    
    render() {

        const { username, useremail, userId } = this.props;
            useremailId=userId;
        return (
            <div className="eze-login-container">
                <Notification  saveNotification={this.saveNotification} />
            </div>

        );
    }
saveNotification=(event,checkbox1,checkbox2,checkbox3,checkbox4)=>{
console.log(checkbox1,checkbox2,checkbox3,checkbox4);
this.props.save_Notification(useremailId,checkbox1,checkbox2,checkbox3,checkbox4);
}
}

const mapStateToProps = ({ userDetails }) => {
    return {
        username: userDetails.name,
        useremail: userDetails.email,
        userId: userDetails.userId
    };
};
const mapDispatchToProps = dispatch => {
    return {
      save_Notification: (useremailId,checkbox1,checkbox2,checkbox3,checkbox4)=>dispatch(saveUserNotification(useremailId,checkbox1,checkbox2,checkbox3,checkbox4))
       
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(notification);