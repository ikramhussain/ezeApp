import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import UserProfile from'../../presentationals/Settings-presentational/profile.component';
 import {saveUser_Profile} from './profile.actionCreater'
let useremailId;
 class Profile extends Component {
    
    render() {

        const { username, useremail, userId } = this.props;
            useremailId=useremail
        return (
            <div className="eze-login-container">
                <UserProfile username={username} useremail={useremail} saveprofile={this.saveprofile} />
            </div>

        );
    }
saveprofile=(event,timezone)=>{
console.log(timezone);
this.props.saveUserprofile(useremailId,timezone);
}
}

const mapStateToProps = ({ userDetails }) => {
    return {
        username: userDetails.name,
        useremail: userDetails.email,
        userId: userDetails.userId
    };
};
const mapDispatchToProps = dispatch => {
    return {
       saveUserprofile: (useremailId,timezone)=>dispatch(saveUser_Profile(useremailId,timezone))
       
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(Profile);