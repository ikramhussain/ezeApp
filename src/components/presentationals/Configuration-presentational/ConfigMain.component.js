import React, {Component} from 'react';
import StepZilla from 'react-stepzilla';
import 'react-stepzilla/src/css/main.css';
import AddTeam from './AddTeamMember.component';
import AddProduct from './AddProduct.component';
import RoutingRules from './RoutingRules.component';
import './css/configMain.css';

class ConfigMain extends Component {
    render() {
        const steps =
            [
            {name: 'Add Team', component: <AddTeam />},
            {name: 'Add Product', component: <AddProduct />},
            {name: 'Routing Rules', component: <RoutingRules />}
            ]
        return(
            <div className='step-progress'>
                <StepZilla steps={steps} />
            </div>
        )
    }
}

export default ConfigMain;