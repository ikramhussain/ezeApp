import React, { Component } from 'react';
import './css/configurationBox.css';

export default class Todo extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
          return (
                <div>
                    <li className="eze-members-list">{this.props.todo.text}
                        <span className="eze-list-cross" onClick={()=>{
                            this.props.remove(this.props.todo.id);
                        }}>
                        x
                        </span>
                    </li>
                </div>
            );
    }
}

//export default Todo;