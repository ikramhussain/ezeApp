import React, { Component } from 'react';
import './css/configurationBox.css';

class TodoForm extends Component{
    constructor(props){
        super(props);
    }
    render(){
        let input;
          return (
            <div>
                <input ref={node => {input = node;}} 
                onKeyPress={(event)=>{this.props.addTodo(event,input.value,input);}}
                list={this.props.list} placeholder={this.props.placeholder} className={this.props.className} />
            </div>
        );
    }
}

export default TodoForm;