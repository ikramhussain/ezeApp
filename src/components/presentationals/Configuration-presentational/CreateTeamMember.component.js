
import React, { Component } from 'react';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import './css/configurationBox.css';
let companyName, employeeName, employeeId,emailId,startTime,endTime,mobilenumber,workingDays=[];
const CreateTeam = ({submit}) => {
    return(
      <div className="config-box">
            
                <div className="form-group">
                    <label htmlFor="ezeCreatecompanyName">Company Name : </label><br />
                    <input type="text" name="eze-Create-companyName" 
                    id="ezeCreatecompanyName" className="form-control eze-Create-companyName" 
                    placeholder="Enter Company Name" 
                     ref={input=>companyName=input} required/>
                </div>    
                
                <div className="form-group">
                    <label htmlFor="ezeCreateEmpid">Employee ID : </label><br />
                    <input type="text" name="eze-Create-empId" 
                    id="ezeCreateEmpid" className="form-control eze-Create-empId"
                     placeholder="Enter Employee ID" 
                      ref={input=>employeeName=input} required />
                </div>    
                
                <div className="form-group">
                    <label htmlFor="ezeCreateName">Name : </label><br />
                    <input type="text" name="eze-Create-name" 
                    id="ezeCreateName" className="form-control eze-Create-name" 
                    placeholder="Enter Employee Name" 
                    ref={input=>employeeId=input} required/>
                </div>

                <div className="form-group">
                    <label htmlFor="ezeCreateEmail">Email ID : </label><br />
                    <input type="email" name="eze-Create-email"
                    id="ezeCreateEmail" className="form-control eze-Create-email" 
                    placeholder="Enter Email ID"  
                    ref={input=>emailId=input} required />
                </div>

                <div className="form-group">
                    <label htmlFor="ezeCreateMobile">Mobile No. : </label><br />
                    <input type="number" name="eze-Create-mobile" 
                    id="ezeCreateMobile" className="form-control eze-Create-mobile"
                     placeholder="Enter Employee Mobile no."  ref={input=>mobilenumber=input} required/>
                </div>

                <div className="form-group">
                    <label htmlFor="ezeCreateWorkingDays">Working Days : </label><br />
                    <select name="eze-Create-workingDays" id="ezeCreateWorkingDays" 
                    className="form-control eze-Create-workingDays"
                    multiple  ref={input=>workingDays=input} required>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                        <option value="Sunday">Sunday</option>
                    </select>
                </div>

                <div className="form-group row">
                    <div className="col-sm-6">
                        <label htmlFor="ezeCreateStartTime">Start Time : </label><br />
                        <TimePicker name="eze-Create-StartTime" 
                        id="ezeCreateStartTime" className="eze-Create-StartTime"
                         ref={input=>startTime=input} required/>
                    </div>
                    <div className="col-sm-6">
                        <label htmlFor="ezeCreateEndTime">End Time : </label><br />
                        <TimePicker name="eze-Create-EndTime"
                         id="ezeCreateEndTime" className="eze-Create-EndTime" 
                          ref={input=>endTime=input} required />
                    </div>
                </div>

                <div className="form-group">
                    <button  name="eze-Create-button" 
                    id="ezeCreatebtn" className="btn btn-primary center-block"
                      onClick={e =>
                     submit(e,companyName.value,
                      employeeName.value
                      ,employeeId.value,emailId.value, startTime.value,
                      endTime.value,mobilenumber.value,workingDays.value)
                      
                      }>Create</button>
                </div>
           
        </div>
        
    );
}

CreateTeam.propTypes = {submit: React.PropTypes.func }

export default CreateTeam;