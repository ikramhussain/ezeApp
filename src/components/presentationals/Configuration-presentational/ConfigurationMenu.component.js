import React, {Component} from 'react';
import CreateTeam from './CreateTeamMember.component';
import ProductLine from './ProductLine.component';


class Setting extends Component{

	render(){
		return(
      <div className="container-fluid">
          <div className="container col-sm-3">
            <h3>Add Configuration</h3>
            <ul className="nav nav-pills nav-stacked">
              <li className="active"><a data-toggle="pill" href="#createTeamMember">Create Team Member</a></li>
              <li><a data-toggle="pill" href="#productLine">Create Product</a></li>
            </ul>
          </div>
          <div className="tab-content col-sm-9">
            <div id="createTeamMember" className="tab-pane fade in active">
              <CreateTeam />
            </div>
            <div id="productLine" className="tab-pane fade">
              <ProductLine />
            </div>
          </div>
        </div>
    )
	}
}

export default Setting;