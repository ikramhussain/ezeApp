import React, { Component } from 'react';
import SelectedMember from './SelectedList.component';
import './css/configurationBox.css';
class TodoList extends Component{
    constructor(props){
        super(props);
    }

    render(){
    const todoNode = this.props.todos.map((todo) => {
        return (<SelectedMember todo={todo} key={todo.id} remove={this.props.remove}/>)
    });
    return (<ul className="eze-members-list-container">{todoNode}</ul>);
    }
}
export default TodoList;