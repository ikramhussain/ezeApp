
import React, { Component } from 'react';
import './css/configurationBox.css';
let productName, productDescription, productFile,productPeriod;
const createProduct = ({addProductLine}) => {
    return (
        <div className="config-box">
                
                    <div className="form-group">
                        <label htmlFor="ezeProductName">Product Name : </label><br />
                        <input type="text" name="eze-product-Name"
                        id="ezeProductName" className="form-control eze-product-Name"
                         placeholder="Enter Product Name" required="true"
                         ref={input=>productName=input} />
                    </div>

                    <div className="form-group">
                        <label htmlFor="ezeProductDescription">Description : </label><br />
                        <textarea name="eze-product-description" 
                        id="ezeProductDescription" 
                        className="form-control eze-product-description"
                         placeholder="Enter Product Description" 
                         required="true" rows="4" 
                         ref={input=>productDescription=input} ></textarea>
                    </div>

                    <div className="form-group">
                        <label htmlFor="ezeProductDemoPeriod">Demo Period : </label><br />
                        <select name="eze-product-Demo-Period" 
                        id="ezeProductDemoPeriod" 
                        className="form-control eze-product-Demo-Period" 
                        required="true"  ref={input=>productPeriod=input}>
                            <option vlaue="1 Hour">1 Hour</option>
                            <option vlaue="2 Hour">2 Hour</option>
                            <option vlaue="3 Hour">3 Hour</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Attatch Manual : </label><br />
                        <input type="file" name="eze-product-Manual" id="ezeProductManual" 
                        className="eze-product-Manual" multiple  ref={input=>productFile=input} />
                        <label htmlFor="ezeProductManual" className="btn btn-default center-block">Click me to Attach manual</label>
                    </div>

                    <div className="form-group">
                        <button  name="eze-Product-Line"
                          className="btn btn-primary center-block" 
                          onClick={e =>
                     addProductLine(e,productName.value,
                      productDescription.value
                      ,productPeriod.value,productFile.value)
                      
                      } >Add</button>
                    </div>
                
        </div>
        
    );
  
}

createProduct.propTypes = {addProductLine: React.PropTypes.func }
export default createProduct;
