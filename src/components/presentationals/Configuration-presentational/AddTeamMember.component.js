import React, { Component } from 'react';
import './css/configurationBox.css';
import SearchMember from './SearchList.component';
import TodoList from './ListContainer.component';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            data :[]
        }
    }
    addTodo(event,val,input){
    // Assemble data
    console.log(event.key);
    if(event.key == 'Enter'){
        console.log(val);
        const todo = {text: val, id: val}
        // Update data
        this.state.data.push(todo);
        // Update state
        this.setState({data: this.state.data});
        console.log(this.state.data[0].text);
        input.value='';
    }
  }
  handleRemove(id){
    // Filter all todos except the one to be removed
    const remainder = this.state.data.filter((todo) => {
      if(todo.id !== id) return todo;
    });
    // Update state with filter
    this.setState({data: remainder});
  }
  render() {
    return (
        <div className="eze-config-container">
            <div className="eze-selected-members eze-add-member-input">
                <TodoList 
                todos={this.state.data} 
                remove={this.handleRemove.bind(this)}
                />  
            </div>

            <div className="eze-add-member-input">
                <SearchMember addTodo={this.addTodo.bind(this)}
                className="eze-search-box"
                placeholder="Enter Name"
                list="searchMember"
                />
                <datalist id="searchMember">
                    <option value="Jatin Bansal" />
                    <option value="Vishal Gupta" />
                    <option value="Amanpreet" />
                    <option value="Sushant" />
                    <option value="Nupur" />
                    <option value="ikram" />
                </datalist>
            </div>
        </div>

        
    );
  }
}


export default App;