import React, { Component } from 'react';
import ProfileSetting from './setting-menu.component';
import ConfigSetting from '../Configuration-presentational/ConfigurationMenu.component';

const SettingMain = () => {
    return(
        <div className="">
            <ul className="nav nav-tabs">
                <li className="active"><a data-toggle="tab" href="#Profile">Profile</a></li>
                <li><a data-toggle="tab" href="#Configuration">Configuration</a></li>
            </ul>
            <div className="tab-content">
                <div id="Profile" className="tab-pane fade in active">
                    <ProfileSetting />
                </div>
                    
                <div id="Configuration" className="tab-pane fade">
                    <ConfigSetting />
                </div>
            </div>
        </div>
    );
}

export default SettingMain;