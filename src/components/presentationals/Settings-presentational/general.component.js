import React, { Component } from 'react';
import './css/general.css';
import './css/settingBox.css';
import Photo from './images/profile.png';
 
 import InviteTeam from'../../containers/settings-container/Genral.component';
const Home = () => {		
	return (
		<div>
			<div className="box">
				<div className="container-fluid">
					<div className="form-group">
						<label htmlFor="txtTeamName">Team Name</label>
						<input type="text" name="txtTeamName" id="txtTeamName" className="form-control" />
					</div>
					
					<div className="form-group">
						<button name="btnTeamName" id="btnTeamName" className="btn btn-success team-name-btn">Save</button>
					</div>
				</div>
			</div>

			<div className="box">
				<div className="container-fluid">
					<div className="">
						<label htmlFor="txtTeamName">Company Logo</label>
						<hr />
						<p>Add your company logo to custom brand your meeting notes.</p>
						<div className="draglogo">Drag and drop your logo, or <label htmlFor="genFile" className="genlabelfile">select a file</label><input type="file" name="genFile" id="genFile" className="genFile" /></div>
					</div>
				</div>
			</div>

			<div className="box">
				<div className="container-fluid">
					<div className="row">
					<ul className="nav nav-tabs">
						<li className="tmlist">Team</li>
						<li className="active"><a data-toggle="tab" href="#Member">Member</a></li>
						<li><a data-toggle="tab" href="#Disabled">Disabled</a></li>
						<li><a data-toggle="tab" href="#Pending">Pending</a></li>
						<li className="tmlistr"><p data-toggle="modal" data-target="#myModal" className="genp"><b>+</b>Invite Team</p></li>
					</ul>
					</div>
					<div className="tab-content">
						<div id="Member" className="tab-pane fade in active">
							<div className="row">
								<div className="col-xs-2 genm"><img src={Photo} className="genimg" /></div>
								<div className="col-xs-7 genm">Vishal Gupta<br />vishal@eze.ai</div>
								<div className="col-xs-3 genm">Owner</div>
							</div>
						</div>

						<div id="Disabled" className="tab-pane fade">
							<div className="row">
								<div className="col-xs-2 genm"><img src={Photo} className="genimg" /></div>
								<div className="col-xs-7 genm">Jatin<br />jatin@eze.ai</div>
								<div className="col-xs-3 genm">Owner</div>
							</div>
						</div>

						<div id="Pending" className="tab-pane fade">
							<div className="row">
								<div className="col-xs-2 genm"><img src={Photo} className="genimg" /></div>
								<div className="col-xs-7 genm">Ikram Gupta<br />ikram@eze.ai</div>
								<div className="col-xs-3 genm">Owner</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div className="modal fade" id="myModal" role="dialog">
			
				<InviteTeam/>
			</div>
		</div>
	);
}

export default Home;