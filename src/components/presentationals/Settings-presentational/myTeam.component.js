import React, { Component } from 'react';
import './css/myTeam.css';
import './css/settingBox.css';

const Home = () => {
	return (
		<div className="box">
			<div className="container-fluid">
				<div className="row smt">
					<div className="col-xs-3"><p>Team 1<br />1 Member</p></div>
					<div className="col-xs-6"></div>
					<div className="col-xs-3 smtact">Active</div>
				</div>
			</div>
		</div>
	);
}

export default Home;