import React, { Component } from 'react';
import './css/notifications.css';
import './css/settingBox.css';
let checkbox1,checkbox2,checkbox3,checkbox4,checkbox5;
const notification = ({saveNotification}) => {
	return (
		<div className="box">
			<div className="container-fluid">
				<div className="row">
					<div className="col-xs-10">
						<h5>Daily Meeting Prep Email</h5>
						<p className="notptxt">A daily email of your upcoming meetings with key info and agendas.</p>
					</div>
					<div className="col-xs-2"><input type="checkbox" className="notchk" ref={onchange => checkbox1 = onchange }/></div>
				</div>
			
				<div className="row">
					<div className="col-xs-10">
						<h5>Meeting About to Start Email</h5>
						<p className="notptxt">An email is sent to all invitees 15 minutes before a meetings starts with the agenda and a link to join the meeting page and take notes together. This email is only sent for meetings that have agenda items added, and can be turned off/on at the individual meeting level as well.</p>
					</div>
					<div className="col-xs-2"><input type="checkbox" className="notchk" ref={input =>checkbox2 = input }/></div>
				</div>
				
				<div className="row">
					<div className="col-xs-10">
						<h5>Meeting Tips and Advice</h5>
						<p className="notptxt">An occasional email sharing tips and advice on running more effective meetings.</p>
					</div>
					<div className="col-xs-2"><input type="checkbox"className="notchk" ref={input => checkbox3 = input }/></div>
				</div>
				
				<div className="row">
					<div className="col-xs-10">
						<h5>Meeting Feedback</h5>
						<p className="notptxt">An email is sent to all invitees requesting feedback after any meeting (of which you are the organizer) that has an agenda.</p>
					</div>
					<div className="col-xs-2"><input type="checkbox" className="notchk"ref={input => checkbox4 = input } /></div>
				</div>
				
				<hr />
				
				<div className="row">
					<div className="col-xs-10">
						<h5>Unsubscribe from all EZE Meeting Notes emails</h5>
					</div>
					<div className="col-xs-2"><input type="checkbox" className="notchk" ref={input => checkbox5 = input }/></div>
				</div>
				
				
				<div className="form-group">
					<button className="notbtn btn btn-success"onClick={e => saveNotification(e, checkbox1.checked,checkbox2.checked,checkbox3.checked,checkbox4.checked,checkbox5.checked)}>Save Changes</button>
				</div>
				
			</div>
		</div>
	);
}
notification.propTypes = { saveNotification: React.PropTypes.func}

export default notification;