import React, { Component } from 'react';
import './css/meetings.css';
import './css/settingBox.css';

const Home = () => {
	return (
		<div className="box">
		<div className="container-fluid">
			<div className="form-group">
				<h5>Default Meeting Access Level</h5>
				<p>This determines who can view the meeting agendas and notes you create. (You can change this at the individual meeting level as well)</p>
			</div>
			<div className="form-group">
				<select className="form-control">
					<option>Open - Anyone with a link can join your meetings.</option>
					<option>Closed - Only the attendees can join your meetings.</option>
					<option>Team - The attendees can join your meetings and your team has read-onli access.</option>
				</select>
			</div>
			<hr />
			<div className=""><button className="btn btn-info mbtn">Save Changes</button></div>
		</div>
		</div>
	);
}

export default Home;