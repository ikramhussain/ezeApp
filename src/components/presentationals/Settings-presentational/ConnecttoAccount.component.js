import React, { Component } from 'react';
import './css/ConnecttoAccount.css';
import './css/settingBox.css';
import Slack from './images/slack.png';
import Asana from './images/asana.png';
import Google from './images/google.png';
import Trello from './images/trello.png';

const Account = () => {
	return (
		<div className="box">
			<div className="container-fluid"> 
				<div className="row accHead">
					<div className="col-xs-12"><b>Connect to Accounts</b></div>
				</div>
				<div className="row accs">
					<div className="col-xs-2 accs-pad"><img src={Slack} className="img" /></div>
					<div className="col-xs-7 accs-pad">Slack</div>
					<div className="col-xs-3"><button className="btn btn-info">Connect</button></div>
				</div>
				<div className="row accs">
					<div className="col-xs-2 accs-pad"><img src={Asana} className="img" /></div>
					<div className="col-xs-7 accs-pad">Asana</div>
					<div className="col-xs-3"><button className="btn btn-info">Connect</button></div>
				</div>
				<div className="row accs">
					<div className="col-xs-2 accs-pad"><img src={Trello} className="img" /></div>
					<div className="col-xs-7 accs-pad">Trello</div>
					<div className="col-xs-3"><button className="btn btn-info">Connect</button></div>
				</div>
				<div className="row accs">
					<div className="col-xs-2 accs-pad"><img src={Google} className="img" /></div>
					<div className="col-xs-7 accs-pad">Google Contacts</div>
					<div className="col-xs-3"><button className="btn btn-info">Connect</button></div>
				</div>
				<div className="row accs">
					<div className="col-xs-2 accs-pad"><img src={Google} className="img" /></div>
					<div className="col-xs-7 accs-pad">Google Drive</div>
					<div className="col-xs-3"><button className="btn btn-info">Connect</button></div>
				</div>
			</div>
		</div>
				
	);
}

export default Account;