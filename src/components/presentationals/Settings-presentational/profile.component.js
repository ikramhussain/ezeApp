import React, { Component } from 'react';
import './css/profile.css';
import './css/settingBox.css';
import Photo from './images/profile.png';
let  timezone;
const Home = ({username,useremail,saveprofile}) => {
	return (	
		<div className="box">
			
			<div className="container-fluid">
				<div className="row">
					<div className="col-xs-3"><img src={Photo} className="simg" /><br /><input type="file" name="userImg" id="userImageUpload" className="changedp" /><label htmlFor="userImageUpload" className="chngDp">Change</label></div>
					<div className="col-xs-9">
						<div className="col-xs-12 form-group">
							<label>Full Name</label><br />
							<input type="text" className="form-control" value={username}  readOnly/>
						</div>
						<div className="col-xs-12 form-group">
							<label>E-mail</label><br />
							<input type="email" readOnly className="form-control"value={useremail}  readOnly/>
						</div>
						<div className="col-xs-12 form-group">
							<label>Time Zone</label><br />
							<select className="form-control" ref={input => timezone = input }>
								<option value="">----Select One----</option>
								<option value="India">India</option>
							</select>
						</div>
						<div className="col-xs-12 form-group">
							<button className="btn btn-info saveBtn"onClick={e => saveprofile(e, timezone.value)}>Save Profile</button>
						</div>
					</div>
				</div>
			</div>
		</div>
						
	);
}
Home.propTypes = { saveprofile: React.PropTypes.func}
export default Home;