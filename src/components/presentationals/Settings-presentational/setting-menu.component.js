import React, {Component} from 'react';
import Profile from '../../containers/settings-container/Profile.component';
import Notification from '../../containers/settings-container/Notification.component';
import Account from './ConnecttoAccount.component';
import Meetings from './meetings.component';
import Myteam from './myTeam.component';
import General from './general.component';

const Setting = () =>{
  return (
        <div className="">
          <div className="col-xs-3">
          <h3>Profile Setting</h3>
            <ul className="nav nav-pills nav-stacked">
              <li className="active"><a data-toggle="pill" href="#profile" className="active">Profile</a></li>
              <li><a data-toggle="pill" href="#notifications">Notifications</a></li>
              <li><a data-toggle="pill" href="#Account">Connect to Account</a></li>
              <li><a data-toggle="pill" href="#meetings">Meetings</a></li>
              <li><a data-toggle="pill" href="#general">General</a></li>
              <li><a data-toggle="pill" href="#myteam">My Team</a></li>
            </ul>
          </div>
          <div className="tab-content col-xs-9">
            <div id="profile" className="tab-pane fade in active">
              <Profile />
            </div>
            <div id="notifications" className="tab-pane fade">
              <Notification />
            </div>
            <div id="Account" className="tab-pane fade">
              <Account />
            </div>
            <div id="meetings" className="tab-pane fade">
              <Meetings />
            </div>
            <div id="general" className="tab-pane fade">
              <General />
            </div>
            <div id="myteam" className="tab-pane fade">
              <Myteam />
            </div>
          </div>
        </div>
  );
}

export default Setting;