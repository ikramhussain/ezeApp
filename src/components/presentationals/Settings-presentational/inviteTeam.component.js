import React, { Component } from 'react';
import './css/general.css';
import './css/settingBox.css';
import Photo from './images/profile.png';
let input;
const Inviteteam = ({ addTodo, pardata,removeuser }) => {
    return (
        <div>

            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Invite Team</h4>
                    </div>
                    <div className="modal-body">
                        <p className="gen-mod-b">Invite the people you work with to EZE Meeting Notes so you can all enjoy more effective meetings.</p>
                        <div className="genRecipients">
                            <p>Recipients</p>
                            <hr className="genhr" />
                           <div className="form-group row">
                                        <div className="col-sm-12"> 
                                            <ul className="eze-members-list-container">
                                 {pardata.map(userlist => {

                                return (<div key={userlist.id} >
                      <li className="eze-members-list">{userlist.text}
                        <span className="eze-list-cross"onClick= {(event)=>removeuser(event,userlist,userlist.id)}>
                            x
                        </span>
                    </li>
                  
                </div>)
                            })
                            }
                </ul>
                    </div>
                    </div> <div className="form-group">
                                <label htmlFor="txtgenemail">Invite by Email</label>
                                <input ref={node => { input = node; }} onKeyPress={(event) => addTodo(event, input.value, input)
                                } list="searchMember" className="form-control" />
                                <datalist id="searchMember">
                                    <option value="JatinBansal@eze.ai" >Jatin</option>
                                    <option value="VishalGupta@eze.ai" >VishalGupta </option>
                                    <option value="Amanpreet@eze.ai" >Amanpreet</option>
                                    <option value="Sushant@eze.ai">Sushant </option>
                                    <option value="Nupur@eze.ai">Nupur </option>
                                    <option value="ikram@eze.ai">ikram </option>
                                    <option value="sumit@eze.ai">Sumit </option>
                                </datalist>
                            </div>
                            <div className="form-group">
                                <label htmlFor="txtgennote">Custom Note (Optional)</label>
                                <textarea name="txtgennote" id="txtgennote" placeholder="Hi, Please join my team on EZE meeting notes so we can run more effective meetings." rows="3" className="form-control" />
                            </div>
                            <div className="form-group">
                                <button name="genformbtn" className="btn btn-info btn-lg">Send</button>
                                <button name="genformbtncncl" className="btn btn-default btn-lg" data-dismiss="modal">Cancel</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}
Inviteteam.propTypes = { addTodo: React.PropTypes.func,removeuser:React.PropTypes.func }
export default Inviteteam;