import React, { Component } from 'react';
import './Forgot.css';
let password, confirm_password;
const  forgotPassword= ({password,confirm_password,passwordSubmit}) => (

		
			<div>
				<div className="box">
				<div className="container-fluid">
				<div className="row">
				<div className="col-xs-12 forgot">
				<label className="pass">Forgot Password</label>
				</div>
				<div className="col-xs-9">
					
					<div className="col-xs-12 form-group forgot">
					<label className="eze-setting-label">New Password</label><br />
							<input type="password" id="password" ref={input => password = input }className="form-control" />
					</div>
					<div className="col-xs-12 form-group forgot">
					<label className="eze-setting-label">Confirm Password</label><br/>
						<input type="password" id="confirm_password"ref={input => confirm_password = input }className="form-control"/>
					</div>

					<div className="col-xs-12 form-group forgot">
						<button className="btn btn-info saveBtn" onClick={e => passwordSubmit(e, password.value, confirm_password.value)}>Submit</button>
					</div>
				</div>
				</div>
				</div>
				</div>
			</div>
			

)

forgotPassword.propTypes = {passwordSubmit:React.PropTypes.func}
	export default forgotPassword;
