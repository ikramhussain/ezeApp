
import React,{Component} from 'react';

import events from './events';
import BigCalendar from 'react-big-calendar';
import Week from 'react-big-calendar/lib/Week';
import dates from 'react-big-calendar/lib/utils/dates';
import localizer from 'react-big-calendar/lib/localizer';
import TimeGrid from 'react-big-calendar/lib/TimeGrid';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { connect } from 'react-redux';
import { getEvents } from '../../containers/login-container/Login.actionCreators';

import { browserHistory } from 'react-router';
import './css/EditMeeting.css';
BigCalendar.setLocalizer(

  BigCalendar.momentLocalizer(moment)
);



class Page extends Component{

   constructor (props) {
    super(props)
    this.state = { modalActive: false }
    
  }

     componentWillMount() {
      let userId=sessionStorage.getItem('userId');

       this.props.getEvents(userId)
       
    }
    openModel (event) {
      this.setState({event});
    console.log(event);
    this.setState({ modalActive: true })
     
  }
  componentDidUpdate(){
     $('#popup').css('display','inline');     
    $('#popup').css("position", "absolute");
     $('#popup').css('left',event.pageX);
    
    $('#popup').css('top',event.pageY);  
  }
  closeModel(event){
    
    this.setState({modalActive:false})
  }

  
   eventStyleGetter(event, start, end, isSelected) {
   // var backgroundColor = "#000000";

    var backgroundColor =event.hexColor;
    var style = {
        backgroundColor: backgroundColor,
        borderRadius: '0px',
        opacity: 0.8,
        color: 'red',
        border: '0px',
        display: 'block'
    };
   
    return { style: style };
  }
 
 handleMeetingClick(event) {
 
  browserHistory.push({
  pathname: 'editevent',
  state: { event: event }
})
 
}
  render(){
    
    return (
      <div>
        
      <BigCalendar
        
         events={this.props.events}
      
         views={['week']}
        defaultView='week'
      
        defaultDate={new Date()}
   //    onSelectEvent={event =>this.popup(event)}
        onSelectEvent={event =>{::this.openModel(event)}}
      eventPropGetter={(this.eventStyleGetter)}/>

      {this.state.modalActive && (

           <div className="arrow_box" id="popup">
            <div className="eze-editMeeting-popup-header">
                <div className="row">
                    <div className="col-xs-10"><label  className="eze-editMeeting-name">{this.state.event.title}</label></div>
                    <div className="col-xs-2"><button className="eze-editMeeting-crossbutton pull-right" onClick={(event=>this.closeModel.bind(this)(event))}>&times;</button></div>
                </div>
            </div>

            <div className="eze-editMeeting-popup-body">
                <label className="eze-editMeeting-label">When</label>
                <p>
                 {this.state.event.start.toString()}
                </p>
                <div className="row">
                    <div className="col-xs-4"><label className="eze-editMeeting-label">Where</label></div>
                    <div className="col-xs-8"><p>{this.state.event.eventLocation}<a href="https://www.google.co.in/maps/@">map</a></p></div>
                </div>

                <div className="row">
                    <div className="col-xs-4"><label className="eze-editMeeting-label">Joining info</label></div>
                    <div className="col-xs-8"><p><a href="/">meet.google.com</a></p></div>
                </div>

                <div className="row">
                    <div className="col-xs-4"><label className="eze-editMeeting-label">Calendar</label></div>
                    <div className="col-xs-8"><p>Vishal Gupta</p></div>
                </div>

                <div className="row">
                    <div className="col-xs-4"><label className="eze-editMeeting-label">Who</label></div>
                    <div className="col-xs-8"><p>Vishal, Ikram</p></div>
                </div>
            </div>
            <div id="pointer">
            </div>

            <div className="eze-editMeeting-popup-footer">
                <div className="row">
                    <div className="col-xs-2"><label className="eze-editMeeting-label">Going?</label></div>
                    <div className="col-xs-4"><p><a href="/">Yes</a> <a href="/">Maybe</a> <a href="/">No</a></p></div>
                    <div className="col-xs-2"><button className="eze-editMeeting-btn">Remove</button></div>
                    <div className="col-xs-4"><button className="eze-editMeeting-btn btn-info"onClick={(event=>this.handleMeetingClick.bind(this)(this.state.event))}>More Details</button></div>
                </div>
            </div>
    </div>

        )}

     </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
        events: state.events,

        UserDetail:state.UserDetail
      
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getEvents: (userId) => dispatch(getEvents(userId))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Page);
 
