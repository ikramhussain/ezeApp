import React,{ Component} from 'react';


import BigCalendar from 'react-big-calendar';

import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { connect } from 'react-redux';

BigCalendar.setLocalizer(

  BigCalendar.momentLocalizer(moment)
);



class CalendarDay extends Component {
   eventStyleGetter(event, start, end, isSelected) {
    var backgroundColor = event.hexColor;

    var backgroundColor =event.hexColor;
    var style = {
        backgroundColor: backgroundColor,
        borderRadius: '0px',
        opacity: 0.8,
        color: 'red',
        border: '0px',
        display: 'block'
    };
   
    return { style: style };
  }
  handleSelect(info) {
    console.log(info.start.toLocaleString())
  }                     
    handleEventSelect(event, _this) {
      console.log(event)
    return (<Popover id="popover-positioned-right" title="Popover right">
        <strong>Holy guacamole!</strong> Check this info.
      </Popover>);
  }
  render(){
    
    return (
       
      <BigCalendar
        events={this.props.events}
           
         views={['day']}
        defaultView='day'
      
        defaultDate={new Date()}
       onSelectEvent={event => alert(event. desc)}
      eventPropGetter={(this.eventStyleGetter)}
        />
     
    )
  }
}

const mapStateToProps = (state) => {
    return {
        events: state.events,

      
    };
};


export default connect(mapStateToProps, null)(CalendarDay) 