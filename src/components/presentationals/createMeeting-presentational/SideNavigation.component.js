import React, { Component } from 'react';
import { browserHistory } from'react-router';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

import './css/Menu.css';

import Logoicon from './images/whitelogo.png';
import Calendaricon from './images/calendaricon.png';
import Configicon from './images/configicon.png';
import Settingicon from './images/settingicon.png';
import Taskicon from './images/Taskicon.png';
import Bell from './images/notification.png';
import DP from './images/profile.png';
import CalendarMenu from './images/calendarmenu.png';
import CalenderWeek from'./CalenderWeek.component';
import ConfigMain from '../../containers/configuration-container/CreateTeamMembers.component';
import CreateMeeting from '../../containers/meeting-container/CreateMeeting.component';
import SettingsMenu from '../Settings-presentational/SettingMain.component';
import CalenderDay from'./CalenderDay.component';
class App extends Component {
    constructor(props){
        super(props);
     this.state={
		 GetVisible:false,
			};
            this.GoogleClick=this.GoogleClick.bind(this);
       
    }
	   
GoogleClick() {
    	this.props.logoutHandler();
        
   
}
  render() {
      var meetings = (
		  <Tooltip id="meetings"><strong>Meetings</strong></Tooltip>
        );
	  var tasks = (
		  <Tooltip id="tasks"><strong>Tasks</strong></Tooltip>
		);
	  var settings = (
		  <Tooltip id="settings"><strong>Settings</strong></Tooltip>
        );
        var config = (
		  <Tooltip id="config"><strong>Configurations</strong></Tooltip>
		);
    return (
        <div className="eze-home-container">
            <aside className="eze-side-menu no-padding">
                <div className="row">
                    <div className="col-lg-12"><a href="/"><img src={Logoicon} className="eze-menu-logo center-block" /></a></div>
                </div>
                <ul className="nav nav-tabs nav-stacked" >
                    <li className="active">
                        <OverlayTrigger placement="right" overlay={meetings}>
                            <a data-toggle="tab" href="#MeetingPanel" className="eze-side-list">
                                <img src={Calendaricon} className="center-block" />
                            </a>
                        </OverlayTrigger>
                    </li>

                    <li>
                        <OverlayTrigger placement="right" overlay={tasks}>
                            <a data-toggle="tab" href="#TaskPanel" className="eze-side-list">
                                <img src={Taskicon} className="center-block" />
                            </a>
                        </OverlayTrigger>
                    </li>

                    <li>
                        <OverlayTrigger placement="right" overlay={settings}>
                            <a data-toggle="tab" href="#SettingPanel" className="eze-side-list">
                                <img src={Settingicon} className="center-block" />
                            </a>
                        </OverlayTrigger>
                    </li>

                    <li>
                        <OverlayTrigger placement="right" overlay={config}>
                            <a data-toggle="tab" href="#ConfigPanel" className="eze-side-list">
                                <img src={Configicon} className="center-block" />
                            </a>
                        </OverlayTrigger>
                    </li>
                    
                </ul>
            </aside>
            <section className="">
                <nav className="navbar navbar-default"style={{marginBottom:'0px'}}>
                    <div className="navbar-header"> <a className="navbar-brand" href="/">Meetings</a></div>
                    <div className="navbar-collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><button className="btn btn-danger eze-create-meeting-btn" data-toggle="modal" data-target="#createMeeting">Create</button></li>
                            <li><a><img src={CalendarMenu} onClick={()=>this.handleCalenderVisibilty()} /></a></li>
                            <li><a href="/"><img src={Bell} /></a></li>
                            <li>
                            <a href="/" className="eze-header-dp" data-toggle="dropdown"><img src={DP} /></a>
                            <ul className="dropdown-menu">
                                <li><a href="/">Privacy</a></li>
                                <li><a href="/">Help</a></li>
                                <li role="separator" className="divider"></li>
                                <li><a onClick={this.GoogleClick}>Log Out</a>
                               
                                </li>
                            </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div className="tab-content">
                    <div id="MeetingPanel" className="tab-pane fade in active">
                        {
                     this.state.GetVisible
	   ? <CalenderDay/>
	   : <CalenderWeek />
	   }
    
                    </div>

                    <div id="TaskPanel" className="tab-pane fade">
                       
                    </div>

                    <div id="SettingPanel" className="tab-pane fade">
                        <SettingsMenu />
                    </div>

                    <div id="ConfigPanel" className="container tab-pane fade">
                        <ConfigMain/>
                    </div>
                </div>

                <CreateMeeting />
            </section>
        </div>
    );
  }
   handleCalenderVisibilty(){
	this.setState({GetVisible: !this.state.GetVisible});
 } 
}


export default App;
