import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
//import ParticipantList from'../../containers/meeting-container/p.component';

  let input;

const CreateMeeting = ({addTodo,mettingTitel,sdate,stime,etime,description,slocation,acntType,create,datee,pardata,removeuser,meetings,deleteUserEvent}) => {
    return(
        
         <div>
   {meetings.desc}
    
    <div className="" id="createMeeting" role="dialog">
        <div className="modal-dialog">
        
            <div className="modal-content">
                <form className="modal-body" autoComplete="on">
                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingTitle">Meeting Title : </label></div>
                        <div className="col-sm-7"><input type="text" name="eze-meeting-title" 
                           className="eze-meeting-title form-control"
                           value={meetings.title}
                         id="meetingTitle"
                         onChange={(event) =>mettingTitel(event)} 
                         required="true"/></div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingDate">Date : </label></div>
                        <div className="col-sm-7">
                            <DatePicker dateFormat="YYYY/MM/DD" 
                            id="meetingDate"value={datee=meetings.start} 
                            locale="en-gb"required="true"
                             placeholderText="Select a date !" 
                             className="eze-create-m-datepicker form-control"  
                             onChange={(event) =>sdate(event)}/></div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingStartTime">Start Time : </label></div>
                        <div className="col-sm-7">
                            <TimePicker className="eze-create-m-timepicker" 
                            id="meetingStartTime" required="true"
                             onChange={(event) =>stime(event)}/></div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="">Meeting Duration : </label></div>
                        <div className="col-sm-7">
                            <div className="row form-group">
                                <div className="col-sm-4">
                                    <button className="btn btn-default">15 Min.</button></div>
                                <div className="col-sm-4">
                                    <button className="btn btn-default">30 Min.</button></div>
                                <div className="col-sm-4">
                                    <button className="btn btn-default">45 Min.</button></div>
                            </div>
                            <div className="row form-group">
                                <div className="col-sm-4"><button className="btn btn-default">1 Hr.</button></div>
                                <div className="col-sm-4"><button className="btn btn-default">1:30 Hr.</button></div>
                                <div className="col-sm-4"><button className="btn btn-default">2 Hr.</button></div>
                            </div>
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingEndTime">End Time : </label></div>
                        <div className="col-sm-7">
                            <TimePicker className="eze-create-m-timepicker" 
                            id="meetingEndTime"required="true" 
                            onChange={(event) =>etime(event)} /></div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingDiscription">Discription : </label></div>
                        <div className="col-sm-7">
                            <textarea id="meetingDiscription" 
                            name="eze-meeting-discription" 
                            className="eze-meeting-discription form-control" value={meetings.desc}
                            rows="4"onChange={(event) =>description(event)}></textarea></div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingParticipants">Participants : </label></div>
                        <div className="col-sm-7">
                             <div className="form-group row">
                                        <div className="col-sm-12"> 
                                            <ul className="eze-members-list-container">
                                 {pardata.map(userlist => {

                                return (<div key={userlist.id} >
                      <li className="eze-members-list">{userlist.text}
                        <span className="eze-list-cross"onClick= {(event)=>removeuser(event,userlist,userlist.id)}>
                            x
                        </span>
                    </li>
                  
                </div>)
                            })
                            }
                </ul>
                    </div>
                                 
                         </div>
         <input ref={node => {input = node;}} onKeyPress={(event)=> addTodo(event,input.value,input)
            } list="searchMember"  className="form-control" />
                <datalist id="searchMember">
                    <option value="JatinBansal@eze.ai" >Jatin</option>
                    <option value="VishalGupta@eze.ai" >VishalGupta </option>
                    <option value="Amanpreet@eze.ai" >Amanpreet</option>
                    <option value="Sushant@eze.ai">Sushant </option>
                    <option value="Nupur@eze.ai">Nupur </option>
                    <option value="ikram@eze.ai">ikram </option>
                    <option value="dumit@eze.ai">Sumit </option>
                </datalist>
                        </div>
                    </div>
 
                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingLocation">Location : </label></div>
                        <div className="col-sm-7">
                            <input list="locations" name="meeting-location"value={meetings.eventLocation} className="eze-meeting-location form-control" id="meetingLocation"onChange={(event)=>slocation(event)} />
                            <datalist id="locations">
                                <option value="Delhi">India</option>
                                <option value="Faridabad">Haryana</option>
                                <option value="White House">U.S.A.</option>
                            </datalist>
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-5"><label htmlFor="meetingAccountType">Account Type : </label></div>
                        <div className="col-sm-7">
                            <select id="meetingAccountType" name="eze-meeting-account"value={meetings.accountType} className="form-control"onChange={(event)=>acntType(event)}>
                                <option value="">Select One Account</option>
                                <option value="Google">Google</option>
                                <option value="Outlook">Outlook</option>
                                <option value="EZE">EZE</option>
                            </select>
                        </div>
                    </div>
                    
                    <div className="form-group row">
                        <div className="col-sm-9"><button className="btn-danger btn center-block"onClick={(event)=>create(event)}>Edit</button></div>
                         <div className="col-sm-3"><button className="btn-danger btn center-block">Delete</button></div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

    );
    }

CreateMeeting.propTypes = { addTodo: React.PropTypes.func,mettingTitel:React.PropTypes.func,sdate:React.PropTypes.func,stime:React.PropTypes.func,etime:React.PropTypes.func,description:React.PropTypes.func,slocation:React.PropTypes.func,acntType:React.PropTypes.func,create:React.PropTypes.func,removeuser:React.PropTypes.func,deleteUserEvent:React.PropTypes.func}
export default CreateMeeting;
