export default [
  {
    'title': 'All Day Event',
    'hexColor':"#000000",
    'allDay': true,
    'start': new Date(2015, 3, 0),
    'end': new Date(2015, 3, 0)
  },
  {
    'title': 'Long Event',
     
    'start': new Date(2015, 3, 7),
    'end': new Date(2015, 3, 7),
     'hexColor':"aqua",
  },

  {
    'title': 'DTS STARTS',
    'start': new Date(2015, 2, 13),
    'end': new Date(2015, 2, 20),
     'hexColor':"brown",

  },

  {
    'title': 'DTS ENDS',
    'start': new Date(2015, 8, 6),
    'end': new Date(2015, 8, 6),
     'hexColor':"yellow",
  },

  {
    'title': 'Some Event',
    'start': new Date(2015, 3, 9),
    'end': new Date(2015, 3, 9),
     'hexColor':"tomato",
  },
  {
    'title': 'Conference',
    'start': new Date(2015, 3, 11),
    'end': new Date(2015, 3, 13),
    'desc': 'Big conference for important people',
     'hexColor':"sienna",
  },
  {
    'title': 'Meeting',
    'start':new Date(2015, 3, 12),
    'end': new Date(2015, 3, 12),
    'desc': 'Pre-meeting meeting, to prepare for the meeting',
    'hexColor':'rebeccapurple'
  },
  {
    'title': 'Lunch',
     'start':new Date(2015, 3, 12),
    'end': new Date(2015, 3, 12),
      'desc': 'Power lunch',
       'hexColor':'rebeccapurple'
  },
  {
    'title': 'Meeting',
    'start':new Date(2015, 3, 12,10,30,0),
    'end': new Date(2015, 3, 12,12,30,0),
     'hexColor':"navy",
  },
  {
    'title': 'Happy Hour',
    'start':new Date(2015, 3, 12,10,30,0),
    'end': new Date(2015, 3, 12,12,30,0),
   'desc': 'Most important meal of the day',
     'hexColor':"palevioletred",
  },
  {
    'title': 'Dinner',
    'start':new Date(2015, 3, 12,10,30,0),
    'end': new Date(2015, 3, 12,12,30,0),
     'hexColor':"olive",
  },
  {
    'title': 'Birthday Party',
    'start':new Date(2015, 3,3),
    'end': new Date(2015, 3, 3),
     'hexColor':"#07c1e4",
  }
]
