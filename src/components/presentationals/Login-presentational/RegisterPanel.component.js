
import React, { Component,PropTypes } from 'react';

import { Grid, Navbar, Jumbotron, Button } from 'react-bootstrap';
import ImgGoogle from './images/GoogleIcon.png';
import ImgOutlook from './images/OutlookIcon.png';
import ImgEze from './images/EzeIcon.png';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import './css/Login.css';
import Name from './images/name.png';
import Email from './images/email.png';
import DOB from './images/dob.png';
import Password from './images/password.png';
import Retype from './images/retype.png';
import GoogleLogin from'../../containers/login-container/Google.component';
import OutlookLogin from '../../containers/login-container/Outlook.component';
let usernameInput, userEmailInput, userPasswordInput,userRepasswordInput;
const RegistrationPenal = ({Registration, registerationStatus }) => (
      <div className="eze-register-panel container-fluid">
          <div className="row login-input"> 
            <p style={{color:'green',marginLeft:'20%',marginBottom:'5%'}} > {registerationStatus}</p>
            <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
              <img src={Name} className="center-block" />
              </div>
            <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
              <input type="text" name="eze-register-name"
                 className="eze-register-name eze-login-input"
                 required="true"placeholder="Your Name" 
                 ref={input => usernameInput = input}/></div>
          </div>
          <div className="row login-input">
            <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
              <img src={Email} className="center-block" /></div>
            <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
              <input type="email" name="eze-register-email" 
              className="eze-register-email eze-login-input" 
              required="true"placeholder="Your Email" 
             ref={input=>userEmailInput=input}/></div>
          </div>
         
          <div className="row login-input">
            <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
              <img src={Password} className="center-block" /></div>
            <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
              <input type="password" id="password"name="eze-register-password"
              required="true" className="eze-register-password eze-login-input" 
              placeholder="Password" ref={input=>userPasswordInput=input}/></div>
          </div>
          <div className="row login-input">
            <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
              <img src={Retype} className="center-block" /></div>
            <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
              <input type="password" id="confirm_password"name="eze-register-retype"
               required="true"className="eze-register-retype eze-login-input"
                placeholder="Re-Type" ref={input=>userRepasswordInput=input}/></div>
          <span id='message'></span>
          </div>
          <div className="row">
           
           <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
             <button className="eze-register-btn btn btn-success center-block"
             onClick={e => Registration(e, usernameInput.value, userEmailInput.value,userPasswordInput.value,userRepasswordInput.value)} >Registration</button></div>
          
          </div>
          
       
        <div>
         <GoogleLogin/>
          <div id="signin-prompt"href="#" className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <a className="outlook_login btn" href="#" role="button"
					id="connect-button" ><img src={ImgOutlook} className="center-block" /></a>
          <OutlookLogin/>
          </div>
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4"><a href="/"><img src={ImgEze} className="center-block" /></a></div>
        </div>
      </div>
    );
  

RegistrationPenal.propTypes = {Registration: React.PropTypes.func }
export default RegistrationPenal;

