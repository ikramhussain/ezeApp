
import React, { Component } from 'react';
import { Grid, Navbar, Jumbotron, Button } from 'react-bootstrap';
import ImgGoogle from './images/GoogleIcon.png';
import ImgOutlook from './images/OutlookIcon.png';
import ImgEze from './images/EzeIcon.png';
import './css/Login.css';
import Email from './images/email.png';
import Password from './images/password.png';
import { connect } from 'react-redux';
import GoogleLogin from'../../containers/login-container/Google.component';
import OutlookLogin from '../../containers/login-container/Outlook.component';
import {loginAction } from '../../containers/login-container/Login.actionCreators';

let userEmailInput, passwordInput, emailVarification;

const LoginPanel = ({login,forgotUserPassword}) => (
  <div className="eze-login-panel container-fluid">
        <div className="row"><h3 className="eze-signin-with">Sign in with</h3></div>
        <div className="row">
         <GoogleLogin/>
          <div id="signin-prompt"href="#"className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<a className="outlook_login btn" href="#" role="button"
					id="connect-button" ><img src={ImgOutlook} className="center-block" /></a>
		        
            </div> 
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <a href="/"><img src={ImgEze} className="center-block" /></a></div>
        </div>
        <div className="row divider"><hr className="eze-login-hr" />
        <span>OR</span><hr className="eze-login-hr" /></div>
       <form>
        <div className="login-input row">
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <img src={Email} className="center-block" /></div>
          <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <input ref={input =>  userEmailInput = input} 
              type="email" name="eze-login-email"required="true" 
              className="eze-login-email eze-login-input" 
              placeholder="Your Email" /></div>
        </div>
        <div className="login-input row">
          <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <img src={Password} className="center-block" /></div>
          <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <input  ref={input => passwordInput = input } 
           
            type="password" name="eze-login-password"
            required="true" 
            className="eze-login-password eze-login-input"
             placeholder="Your Password" /></div>
        </div>
         <p style={{textAlign:'right'}}><a data-toggle="modal" 
         data-target="#myModal">Forgot Password?</a></p>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
            <input type="button" 
              className="eze-login-btn btn btn-info center-block" value="Login in EZE"
              onClick={e => login(e,  userEmailInput.value, passwordInput.value)} />
          </div>
        </div>
        </form>
         <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog modal-sm">
              <div className="modal-content">
              <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal">&times;</button>
            </div>
            <div className="modal-body">
            <form>

             <div className="form-group">
                  <input type="Email" className="form-control" 
                  placeholder="Enter Email Address" 
                  ref={input => emailVarification = input }required/>
                </div>
                <div className="form-group">
                  <button className="btn btn-info center-block" data-dismiss="modal"onClick={e => forgotUserPassword(e,  emailVarification.value)} >Submit</button>
                </div>

            </form>
   </div>
          </div>
        </div>
      </div>
      </div>
);

LoginPanel.propTypes = { login: React.PropTypes.func,forgotUserPassword:React.PropTypes.func }
export default LoginPanel;

