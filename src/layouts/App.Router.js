import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import AppLayout from './App.layout';
import Login from '../components/containers/login-container/Login.componenet';
import MeetingContainer from '../components/containers/meeting-container/meeting.component'
import { checkAuth } from '../utils/AuthUtils';
import forgotpassword from '../components/containers/password/forgotPassword.component';
import Editevent from '../components/containers/meeting-container/EditEvent.component';
const AppRouter = () => (
	<Router history={browserHistory}>
		<Route path="/" component={AppLayout} >
			<IndexRoute component={Login} />
			<Route onEnter={checkAuth} path="Home" component={MeetingContainer} />
			<Route path="password" component={forgotpassword} />
			<Route path="editevent" component={Editevent} />
		</Route>
	</Router>
);

export default AppRouter;