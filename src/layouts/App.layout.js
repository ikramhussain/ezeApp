import React from 'react';

import { Provider } from 'react-redux';
import configureStore from '../config/store.config';


const initialState = {
	events: [],
	userDetails: {},
	data:[]
};

export default class AppLayout extends React.Component {
	render(){
		return (
			<Provider store={configureStore({ initialState })}>
				<div id="app-container" className="container-fluid" style={{height: '100%'}}>  
					<div className="row" style={{height: '100%'}}>             
						{this.props.children}
					</div>
                </div>
            </Provider>
		);
	}
}