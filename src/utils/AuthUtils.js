
import LocalStorage from './localStorageUtil';

export const isAuthenticated = () => {
    const token = LocalStorage.getFromLocalStorage('ACCESS_TOKEN');
    return token ? true : false;
}; 

export const checkAuth = (nextState, replace) => {
    if(!isAuthenticated()) {
        replace({ pathname: '/' }); 
    }
}