
function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers
 
    if (fld.value == "") {
        fld.style.background = 'Yellow';
        error = "You didn't enter a password.\n";
        alert(error);
        return false;
 
    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
        error = "The password is the wrong length. \n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else if (illegalChars.test(fld.value)) {
        error = "The password contains illegal characters.\n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else if ( (fld.value.search(/[a-zA-Z]+/)==-1) || (fld.value.search(/[0-9]+/)==-1) ) {
        error = "The password must contain at least one numeral.\n";
        fld.style.background = 'Yellow';
        alert(error);
        return false;
 
    } else {
        fld.style.background = 'White';
    }
   return true;
}


 var error = "";
    var illegalChars = /\W/; // allow letters, numbers, and underscores
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

 export const validateRegistrationTextField =(usernameInput,userEmailInput,password,userRepasswordInput, callback)=> {
   if (usernameInput == "") {
        error = "You didn't enter a username.\n";
        alert(error);
        return false;

    }
   else if (!filter.test(userEmailInput)) {
    alert('Please Enter a valid email address!!');
    return false;
  }  
 else if ((password.length< 8) || (password.length> 15)) {
        error = "password length should be 8 to 15 charecter!";
         console.log(error)
        alert(error);
        return false;
    }
     else if (password!== userRepasswordInput) {
        error = "password Not Match";
         console.log(error)
        alert(error);
        return false;

    } else {
       callback(true)
    }
    return true;
}
   export const validateTextField =(userEmailInput,password,callback)=> {
  
    if (!filter.test(userEmailInput)) {
    alert('Please Enter a valid email address!!');
     
    return false;
  }else if ((password.length< 8) || (password.length> 15)) {
        error = "password length should be 8 to 15 charecter!";
         console.log(error)
        alert(error);
        return false;
     }
    else {
       callback(true)
    }
    return true;
}
export const validateForgotPassword =(userNewPassword,userconfirmPassword)=>{
     if ((userNewPassword.length< 8) || (userNewPassword.length> 15)) {
        error = "password length should be 8 to 15 charecter!";
         console.log(error)
        alert(error);
        return false;
    }
     else if (userNewPassword!== userconfirmPassword) {
        error = "password Not Match";
         console.log(error)
        alert(error);
        return false;

    } else {
       callback(true)
    }
    return true;
}
export const validateEmailVarify=(emailVarification,callback)=>{
 if (!filter.test(emailVarification)) {
    alert('Please Enter a valid email address!!');
    return false;
  }else{
  callback(true);
  }
  return true
}
  